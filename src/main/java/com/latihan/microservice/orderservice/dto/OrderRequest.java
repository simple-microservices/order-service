package com.latihan.microservice.orderservice.dto;

import com.latihan.microservice.orderservice.external.dto.PaymentMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequest {
    private Integer quantity;
    private Long amount;
    private Long productId;
    private PaymentMode mode;
}
