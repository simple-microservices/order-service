package com.latihan.microservice.orderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponse {
    private Long id;
    private Integer quantity;
    private Long amount;
    private String status; // Status Pembayaran =>SYSTEM
    private Instant date; // SYSTEM
    private Long productId;
}
