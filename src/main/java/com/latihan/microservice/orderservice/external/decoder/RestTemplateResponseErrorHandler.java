package com.latihan.microservice.orderservice.external.decoder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.latihan.microservice.orderservice.dto.ErrorMessage;
import com.latihan.microservice.orderservice.exception.CustomException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

@Component
@Slf4j
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        log.info("Status code error : {}",response.getStatusCode().value());
        return response.getStatusCode().value() == 400;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        if (response.getStatusCode().value() == 400){
            ObjectMapper objectMapper = new ObjectMapper();
            ErrorMessage errorMessage = objectMapper.readValue(response.getBody(), ErrorMessage.class);
            throw new CustomException(
                    errorMessage.getMessage(),
                    errorMessage.getError(),
                    response.getStatusCode().value()
            );
        }
    }
}
