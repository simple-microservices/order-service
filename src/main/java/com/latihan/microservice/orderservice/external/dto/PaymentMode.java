package com.latihan.microservice.orderservice.external.dto;

public enum PaymentMode {
    CASH,
    PAYPAL,
    CREDIT_CARD,
    DEBIT_CARD
}
