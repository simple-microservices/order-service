package com.latihan.microservice.orderservice.service.impl;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.latihan.microservice.orderservice.dto.OrderRequest;
import com.latihan.microservice.orderservice.dto.OrderResponse;
import com.latihan.microservice.orderservice.entity.Order;
import com.latihan.microservice.orderservice.exception.CustomException;
import com.latihan.microservice.orderservice.external.dto.PaymentRequest;
import com.latihan.microservice.orderservice.repository.OrderRepository;
import com.latihan.microservice.orderservice.service.OrderService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private RestTemplate restTemplate;

    @Override
    public List<OrderResponse> getAll() {
        return orderRepository.findAll()
                .stream()
                .map(order -> {
                    return mapperOrderToOrderResponse(order);
                }).collect(Collectors.toList());
    }

    @Override
    public OrderResponse getById(Long id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(() -> new CustomException(
                        "Order with id " + id + " not found",
                        "ORDER_NOT_FOUND",
                        404
                ));
        return mapperOrderToOrderResponse(order);
    }

    @Override
    @Transactional
    public OrderResponse create(OrderRequest orderRequest) {

        // Product Service
            // Check Availability dari Product
        restTemplate.exchange(
                "http://PRODUCT-SERVICE/api/products/checkAvailable/" + orderRequest.getProductId() + "/" + orderRequest.getQuantity(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Void>() {
                }
        );
        //Place Order
        Order order = Order.builder()
                .amount(orderRequest.getAmount())// Total Biaya
                .quantity(orderRequest.getQuantity()) // Total Product
                .productId(orderRequest.getProductId())
                .status("CREATED")
                .date(Instant.now())
                .build();

        orderRepository.saveAndFlush(order);

        PaymentRequest paymentRequest = PaymentRequest.builder()
                .orderId(order.getId())
                .mode(orderRequest.getMode())
                .amount(order.getAmount())
                .build();

        // Payment service -> Create
        String orderStatus;
        try {
            restTemplate.exchange(
                    "http://PAYMENT-SERVICE/api/payments",
                    HttpMethod.POST,
                    new HttpEntity<>(paymentRequest),
                    new ParameterizedTypeReference<Void>() {
                    }
            );
            orderStatus = "PLACED";
        }catch (Exception e){
            orderStatus = "PAYMENT_FAILED";
        }

        // ReduceQuantity -> PRODUCT SERVICE
        restTemplate.exchange("http://PRODUCT-SERVICE/api/products/reduceQuantity/" + order.getProductId() + "/" + order.getQuantity(),
                HttpMethod.PUT,
                null,
                new ParameterizedTypeReference<Void>() {
                }
        );


        // Update STATUS PAYMENT in Order
        order.setStatus(orderStatus);
        orderRepository.save(order);


        return mapperOrderToOrderResponse(order);
    }

    @Override
    public OrderResponse update(Long id, OrderRequest orderRequest) {
        getById(id);
        Order order = mapperOrderRequestToOrder(orderRequest);
        order.setId(id);
        orderRepository.saveAndFlush(order);
        return mapperOrderToOrderResponse(order);
    }

    @Override
    public OrderResponse delete(Long id) {
        OrderResponse order = getById(id);
        orderRepository.deleteById(id);
        return order;
    }

    private OrderResponse mapperOrderToOrderResponse(Order order){
        OrderResponse orderResponse = new OrderResponse();
        BeanUtils.copyProperties(order,orderResponse);
        return orderResponse;
    }

    private Order mapperOrderRequestToOrder(OrderRequest orderRequest){
        Order order = new Order();
        BeanUtils.copyProperties(orderRequest,order);
        return order;
    }
}
