package com.latihan.microservice.orderservice.service;

import com.latihan.microservice.orderservice.dto.OrderRequest;
import com.latihan.microservice.orderservice.dto.OrderResponse;

import java.util.List;

public interface OrderService {

    public List<OrderResponse> getAll();
    public OrderResponse getById(Long id);
    public OrderResponse create (OrderRequest orderRequest);
    public OrderResponse update (Long id, OrderRequest orderRequest);
    public OrderResponse delete (Long id);

}
