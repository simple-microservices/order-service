package com.latihan.microservice.orderservice.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.latihan.microservice.orderservice.dto.OrderRequest;
import com.latihan.microservice.orderservice.dto.OrderResponse;
import com.latihan.microservice.orderservice.dto.ResponseMessage;
import com.latihan.microservice.orderservice.service.OrderService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/orders")
@AllArgsConstructor
public class OrderController {

    // @Autowired
    private OrderService orderService;

    @GetMapping
    public ResponseEntity<ResponseMessage<List<OrderResponse>>> getAll(){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(orderService.getAll());
        response.setMessage("List all orders.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseMessage<OrderResponse>> getById(@PathVariable Long id){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(orderService.getById(id));
        response.setMessage("Get Detail order.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ResponseMessage<OrderResponse>> create(@RequestBody OrderRequest orderRequest){
        ResponseMessage response = new ResponseMessage<>();
        response.setData(orderService.create(orderRequest));
        response.setMessage("Success create order.");
        response.setSuccess(true);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

//    @PutMapping("/{id}")
//    public ResponseEntity<OrderResponse> update(@PathVariable Long id, @RequestBody OrderRequest orderRequest){
//        return new ResponseEntity<>(orderService.update(id,orderRequest),HttpStatus.OK);
//    }

//    @DeleteMapping("/{id}")
//    public ResponseEntity<OrderResponse> delete(@PathVariable Long id){
//        return new ResponseEntity<>(orderService.delete(id),HttpStatus.OK);
//    }

 }
